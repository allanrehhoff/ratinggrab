<?php
	function multiexplode($delimiters, $string) {
		$ready = str_replace($delimiters, $delimiters[0], $string);
		$launch = explode($delimiters[0], $ready);
		return  $launch;
	}

	function row($title = '', $rating = '', $metascore = '') {
		return sprintf($GLOBALS["header_mask"], $title, $rating, $metascore).CRLF;
	}

	function fullrow($content) {
		// Minus the one space prepended at the start, serving as padding.
		return str_pad("| ".$content, rowlength() - 1, SPACE).'|'.CRLF;
	}

	function rowlength() {
		// Substract the length of a line ending appended in row();
		// Because those aren't actually a part of the length.
		return mb_strlen(row()) - mb_strlen(CRLF);
	}

	function divider() {
		return "+".str_repeat('-', 52)."+".str_repeat("-", 8)."+".str_repeat("-", 13)."+".CRLF;
	}

	function write($out) {
		if($GLOBALS["logfile"] !== false) {
			$log = preg_replace('#\\x1b[[][^A-Za-z]*[A-Za-z]#', '', $out);
			file_put_contents($GLOBALS["logfile"], $log, FILE_APPEND);
		}

		print $out;
	}

	function rrmdir($dir) { 
		if (is_dir($dir)) { 
			$objects = scandir($dir); 
			foreach ($objects as $object) { 
				if ($object != "." && $object != "..") { 
					if (is_dir($dir."/".$object)) {
						rrmdir($dir."/".$object);
					} else {
						unlink($dir."/".$object); 
					}
				}
			}

			rmdir($dir); 
		} 
	}

	if(function_exists("shell_exec") && $allow_python) {
		$python = `python --version 2>&1`;
		$has_python = preg_match("/Python (.*)/", $python, $python_version);

		if($has_python && version_compare("2.7", $python_version[1])) {
			function parse_movie($torrent) {
				$torrent = escapeshellarg($torrent);
				exec("python parser.py --file $torrent", $parsed, $return);

				if($return !== 0) {
					die("Oh god no! Python has quit unexpectedly... ".CRLF."Run the following command to debug... ".CRLF.TAB."python parser.py --file \"test file\"".CRLF.CRLF."Alternatively consider disallowing python in script configurations.".CRLF."Doing so will produce less accurate results.".CRLF);
				}

				$moviedata = json_decode($parsed[0]);
				$moviename = '';

				if(json_last_error() === JSON_ERROR_NONE) {
					$moviename = $moviedata->title;
				}

				return $moviename;
			}
		}
	}

	if(!function_exists("parse_movie")) {
		function parse_movie($filename) {
			$words = multiexplode($GLOBALS["moviename_delimiters"], $filename);
			$words = array_filter($words, function($var) {
				return !(preg_match("/(?:HDTV|\w{2,3}rip)|(?:x264)|(?:\d{4})|(?:\d{3,4}p)|(?:AC\d)/i", $var));
			});
			
			$moviename = trim(str_ireplace($GLOBALS["movieame_bogus"], '', join(' ', $words).CRLF));
			return $moviename;
		}
	}