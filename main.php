<?php
	// Absolutely no trailing slashes, everytime you write a trailing slash, god kills a kitten!
	$movie_directories	  = ['../../Plex/WD1/Movies', '../../Plex/WD2/Movies', '../../Plex/WD3/Movies'];

	$rating_bad 		  = 5; // Less than
	$rating_good		  = 7; // Greater than

	// Allow python to be used for parsing movie names from files.
	// Produces more accurate results, but comes with a performance penalty.
	$allow_python		  = true;

	// Set this to true to be asked whether or not a bad rated movie
	// should be deleted prior to continue analyzing.
	$ask_to_delete		  = false;

	// Set to false to disable logging of output, disabling this will increase I/O performance.
	$logfile			  = "rating.log";

	$supported_extensions = ['mp4', 'mkv', 'avi', 'iso', 'img'];
	$moviename_delimiters = ['.', ' ', '-'];
	$movieame_bogus 	  = [
		'DVDR', 'anoXmous', 'IMAX', '+HI', 'cam', 'ts',
		'AAC', 'H264', 'Rx', 'REMASTERED', 'dxva-HDLiTE', 'UNiTAiL',
		'(Phoenix-RG)', '(HDScene-Release)', 'HD-', 'XVID', 'Hive-CM8',
		'YIFY', 'WEB-DL', 'WEB DL', 'DKsubs', 'BOKUTOX', 'fgt',
		'[Uncut]', 'XViD', 'juggs', '[ETRG]', 'ETRG', 'danish',
		'DTS', 'UNRATED', 'EXTENDED', 'DIRECTORS CUT', 'UNRATED',
		'SONiDO', 'UNiTY', 'torrentz', '3xform', 'TheFalcon007',
		'RARBG', 'aXXo', 'bluray', 'GAZ', 'FXG', 'iNFAMOUS',
		'NTSC', 'Custom', 'sparks', 'yts', '3li', 'IpadX', 'mp4',
		'1080p', '720p', 'WEBRip', 'SUBSTANCE'
	];
	$header_mask		  = '| %-50s | %6s | %11s |';

	/**
	* You should not need to edit below this comment
	* Unless you for some bizarre reaspm knows what you're doing,
	* or for whatever reason just wants to see the world burn.
	*/
	define("CR", "\r");
	define("LF", "\n");
	define("TAB", "\t");
	define("CRLF", CR.LF);
	define("DS", DIRECTORY_SEPARATOR);
	define("SPACE", chr(32)); // Hacky I know, and i'm not sorry.

	spl_autoload_register(function($class) {
		require __DIR__.DS.$class.".class.php";
	});

	chdir(__DIR__);
	file_put_contents($logfile, '');

	require __DIR__.DS."functions.php";

	if(version_compare(PHP_VERSION, "5.4", "<")) die("The PHP version required to run this is 5.4".CRLF);
	if(!extension_loaded('curl')) die("cURL extension is not enabled, unable to proceed.");
	if(!extension_loaded("mbstring")) die("mbstring extension is not enabled, unable to proceed.");
	if (function_exists("passthru")) passthru("clear");

	if(in_array("--debug", $argv)) {
		print "Script directory  : ".__DIR__.CRLF;
		print "User invoked      : ".get_current_user().CRLF;
		print "PHP Version       : ".phpversion().CRLF;

		$movie_directories = array_map("realpath", $movie_directories);
		print "Movie directories : ".implode(', ', $movie_directories).CRLF;
		exit;
	}

	$color			   = new Colors();
	$analyze_starttime = microtime(true);
	$header			   = row("Title", "Rating", "Votes");
	$skipped_folders   = [];

	foreach($movie_directories as $movie_directory) {
		if(!is_dir($movie_directory)) {
			print $color->red($movie_directory." No such file or directory").CRLF;
			continue;
		}

		$movie_directory = realpath($movie_directory);
		$folders = new RecursiveDirectoryIterator($movie_directory, RecursiveDirectoryIterator::SKIP_DOTS);

		write(divider());
		write(fullrow("Directory: ".$movie_directory));
		write(divider());
		write($header);
		write(divider());

		foreach($folders as $folder) {
			if(is_dir($folder)) {
				$got_valid_file = false;
				$folderfiles = scandir($folder);

				foreach($folderfiles as $file) {
					$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));

					if(in_array($ext, $supported_extensions)) {
						$filepath = $folder.'/'.$file;
						$handle = fopen($filepath, "r");
						
						if($handle) {
							$pathinfo = pathinfo($file);
							$filename = $pathinfo['filename'];
							
							$moviename = parse_movie($filename);
							
							if($moviename == '') {
								write($color->red('Skipping '.$filename.' name was empty after parsing.').CRLF);
								write(divider());
								continue;
							}

							$imdb = new IMDB($moviename);

							if(!$imdb->isReady) {
								write($color->red('Failed fetching information for '.$moviename));
								write(divider());
								continue;
							}
	 
							$raw_rating = floatval($imdb->getRating());
							$rating = str_pad($raw_rating, mb_strlen("Rating"), SPACE, STR_PAD_LEFT);

							if($raw_rating == 0) {
								write($color->red("Skipped false positive...").CRLF);
								write(divider());
								continue;
							}
							if($rating >= $rating_good) {
								$rating = $color->green($rating);
							} else if($rating >= $rating_bad) {
								$rating = $color->yellow($rating);
							} else if($rating < $rating_bad) {
								$rating = $color->red($rating);
							}

							//$rating = str_replace([CR, LF], '', $rating);
							$result = $imdb->getTitle();
							$votes  = $imdb->getVotes();

							write(row($result, $rating, $votes));
							write(divider());

							if($ask_to_delete == true && $raw_rating < $rating_bad ) {
								print "Delete directory for: ".$filepath." ?".CRLF."[".$color->green("Yes")."/".$color->red("No")."]: ";

								$response = fgets(STDIN);
								$delete = substr(strtolower($response), 0, 1) == "y";

								if($delete === true) {
									rrmdir(dirname($filepath.DS));
								} else {
									print "Lol...".CRLF;
								}

								print divider();
							}
						} else {
							write($color->warning("Unable to open: ".$filepath));
						}

						$got_valid_file = true;
					}
				}

				if($got_valid_file === false) {
					$skipped_folders[] = $folder;
					continue;
				}
			}
		}
	}
	
	$analyze_endtime = microtime(true);
	$microtime_spent_analyzing = round($analyze_endtime-$analyze_starttime, 2);

	write('Time spent analyzing: '.$microtime_spent_analyzing.' seconds.'.CRLF);
	write('Skipped folders: '.count($skipped_folders));