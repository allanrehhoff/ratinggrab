#RatingGrab tool for analyzing movies#

##Documentation##
A command line tool used for (trying to) parse movie names out of a file name and look it up on imdb  
After parsing this script will return the rating value found on imdb for the corrosponding movie, highlighted in either red yellow or green, depending on rating.  

1. Clone this repository to your desired folder.  
2. Edit main.php and adjust configuration variables as needed (Mainly the $movie_directories).  
3. $ cd /path/to/ratinggrab
4. $ php main.php  

Allowing python to be used for parsing movie name will produce significantly more accurate results, but comes with a performance penalty,  
because a new process will have to be spun up for python in order to do so. Sorry...  
However allowing python to be used it no guarantee it will be used, python will only be used if the server supports it.

Results are cached on the disk in the same folder as the tool resides.  

##Requirements##
The following requirements must be met for this tool to function without throwing a hissy fit, or some other unexpected error.

* PHP >= 5.4
* Python 2.7 (Optional, but recommended)
* php-mbstring
* php-curl
* A terminal supporting ANSI colors.

You may check your current PHP version by running.  
$ php --version

Additionally check your python version.  
$ python --version

Some server setups use "php5" or "php7" as an alias for "php" instead.  
So if running "php main.php" does not crack if for you,  
try these commands "$ php5 main.php" or "$ php7 main.php"  

The tool attempts to detect missing requirements, and stops the process if it cannot run.  
Should you be amongst the unlucky few with a server that doesn't meet the requirements, you can install them using those commands.  
(Please note that these commands are generic, and will not work for all setups, Hapopy debugging :D )

$ sudo apt-get install php-curl
$ sudo apt-get install php-mbstring
$ sudo service apache2 restart

Windows users should be able to patch cmd with [AnsiColors](https://github.com/adoxa/ansicon)
However I recommend you using WSL bash or Putty, since the tool have been optimized for these.  

##Permissions##
The user you're invoking this script as must have the following rights.

* Reading rights to every folder defined in $movie_directories.
* Read+Write to the directory where this tool is located (Including subfolders

run `whoami` to find out which user will be invoking this script.
or type `php main.php --debug` to print debug informations