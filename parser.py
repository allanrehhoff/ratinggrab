import PTN
import json, sys, argparse
import signal

signal.signal(signal.SIGINT, signal.SIG_IGN)

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file', dest='file', help="the torrent file name to parse.")

args = parser.parse_args();
result = PTN.parse(args.file);

print(json.dumps(result));